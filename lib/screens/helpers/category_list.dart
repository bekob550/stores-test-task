import 'package:stores_test_task/screens/models/category_models.dart';

List<CategoryModel> listOfCategory = [
  CategoryModel(
    image: 'category1',
    title: 'Строительство и ремонт',
  ),
  CategoryModel(
    image: 'category2',
    title: 'Электросамокаты',
  ),
  CategoryModel(
    image: 'category3',
    title: 'Электроника',
  ),
  CategoryModel(
    image: 'category4',
    title: 'Бижутерии',
  ),
  CategoryModel(
    image: 'category1',
    title: 'Строительство и ремонт',
  ),
  CategoryModel(
    image: 'category2',
    title: 'Электросамокаты',
  ),
  CategoryModel(
    image: 'category3',
    title: 'Электроника',
  ),
  CategoryModel(
    image: 'category4',
    title: 'Бижутерии',
  ),
];

List<CategoryModel> listForKidsCategory = [
  CategoryModel(
    image: 'forKids5',
    title: 'Игрушки',
  ),
  CategoryModel(
    image: 'forKids4',
    title: 'Детское питание',
  ),
  CategoryModel(
    image: 'forKids3',
    title: 'Книги',
  ),
  CategoryModel(
    image: 'forKids2',
    title: 'Одежда',
  ),
  CategoryModel(
    image: 'forKids1',
    title: 'Подгузники',
  ),
  CategoryModel(
    image: 'forKids5',
    title: 'Игрушки',
  ),
  CategoryModel(
    image: 'forKids4',
    title: 'Одежда',
  ),
  CategoryModel(
    image: 'forKids3',
    title: 'Книги',
  ),
];

List<CategoryModel> listMansCategory = [
  CategoryModel(
    image: 'forMan1',
    title: 'Брюки',
  ),
  CategoryModel(
    image: 'forMan2',
    title: 'Пиджаки',
  ),
  CategoryModel(
    image: 'forMan3',
    title: 'Шым-Костюмы',
  ),
  CategoryModel(
    image: 'forMan4',
    title: 'Куртки',
  ),
  CategoryModel(
    image: 'forMan1',
    title: 'Футболки',
  ),
  CategoryModel(
    image: 'forMan2',
    title: 'Брюки',
  ),
  CategoryModel(
    image: 'forMan3',
    title: 'Куртки',
  ),
  CategoryModel(
    image: 'forMan4',
    title: 'Шым-Костюмы',
  ),
];
