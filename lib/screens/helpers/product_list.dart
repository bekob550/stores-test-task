import 'package:stores_test_task/screens/models/product_model.dart';

List<ProductModel> listOfProducts1 = [
  ProductModel(
    isShopName: true,
    isCashBack: true,
    isSalePrice: true,
    salePrice: '3700',
    price: 3100,
    image: 'product1',
    title: 'Футболка Reebok 1221',
  ),
  ProductModel(
    price: 1850,
    isSalePrice: true,
    salePrice: '2150',
    image: 'product2',
    title: 'Блендер Xiaomi Pinlo',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
  ProductModel(
    price: 3100,
    image: 'product1',
    title: 'Футболка Reebok 1221',
  ),
  ProductModel(
    price: 1850,
    image: 'product2',
    title: 'Блендер Xiaomi Pinlo',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
];

List<ProductModel> listOfProducts2 = [
  ProductModel(
    isShopName: true,
    isCashBack: true,
    isSalePrice: true,
    price: 41200,
    salePrice: '44 700',
    image: 'product1',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 41200,
    isSalePrice: true,
    salePrice: '44 700',
    image: 'product2',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
  ProductModel(
    price: 3100,
    image: 'product1',
    title: 'Футболка Reebok 1221',
  ),
  ProductModel(
    price: 1850,
    image: 'product2',
    title: 'Блендер Xiaomi Pinlo',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
];

List<ProductModel> listOfProducts3 = [
  ProductModel(
    isShopName: true,
    isCashBack: true,
    isSalePrice: true,
    salePrice: '44 700',
    price: 41200,
    image: 'product1',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 41200,
    isSalePrice: true,
    salePrice: '44 700',
    image: 'product2',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
  ProductModel(
    price: 3100,
    image: 'product1',
    title: 'Футболка Reebok 1221',
  ),
  ProductModel(
    price: 1850,
    image: 'product2',
    title: 'Блендер Xiaomi Pinlo',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
];

List<ProductModel> listOfProducts4 = [
  ProductModel(
    isShopName: true,
    isCashBack: true,
    isSalePrice: true,
    salePrice: '44 700',
    price: 41200,
    image: 'product1',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 41200,
    isSalePrice: true,
    salePrice: '44 700',
    image: 'product2',
    title: 'Смарт-часы Apple Watch Series 6 44 mm LTE Product Red',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
  ProductModel(
    price: 3100,
    image: 'product1',
    title: 'Футболка Reebok 1221',
  ),
  ProductModel(
    price: 1850,
    image: 'product2',
    title: 'Блендер Xiaomi Pinlo',
  ),
  ProductModel(
    price: 18000,
    image: 'product3',
    title: 'Apple Watch',
  ),
  ProductModel(
    price: 50000,
    image: 'product4',
    title: 'Apple Iphone 11',
  ),
];
