class ProductModel {
  final String image;
  final String title;
  final bool isShopName;
  final int price;
  final bool isCashBack;
  final bool isSalePrice;
  final String? salePrice;

  ProductModel({
    this.isShopName = false,
    required this.price,
    this.salePrice,
    this.isCashBack = false,
    this.isSalePrice = false,
    required this.image,
    required this.title,
  });
}
