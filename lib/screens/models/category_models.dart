class CategoryModel {
  final String image;
  final String title;
  final double width;
  final double height;

  CategoryModel({
    this.width = 77,
    this.height = 77,
    required this.image,
    required this.title,
  });
}
