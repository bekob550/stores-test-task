/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/main_form.dart';
import 'package:stores_test_task/theme/constants.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return const MainForm();
  }
}
