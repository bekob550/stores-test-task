/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/models/product_model.dart';
import 'package:stores_test_task/screens/widgets/product_card.dart';
import 'package:stores_test_task/screens/widgets/product_from_card.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class ProductGridTile extends StatefulWidget {
  final String advertising;
  final bool isProductFrom;
  final List<ProductModel> listOfProducts;

  const ProductGridTile({
    super.key,
    required this.advertising,
    this.isProductFrom = false,
    required this.listOfProducts,
  });

  @override
  State<ProductGridTile> createState() => _ProductGridTileState();
}

class _ProductGridTileState extends State<ProductGridTile> {
  bool showAllButton = false;

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Column(
      children: [
        Stack(
          children: [
            GridView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: showAllButton ? widget.listOfProducts.length : 4,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.64,
                crossAxisSpacing: 9,
                mainAxisSpacing: 13,
              ),
              itemBuilder: (context, index) {
                return ProductCard(
                  salePrice: widget.listOfProducts[index].salePrice,
                  image: widget.listOfProducts[index].image,
                  title: widget.listOfProducts[index].title,
                  price: widget.listOfProducts[index].price,
                  isCashBack: widget.listOfProducts[index].isCashBack,
                  isShopName: widget.listOfProducts[index].isShopName,
                  isSalePrice: widget.listOfProducts[index].isSalePrice,
                );
              },
            ),
            InkWell(
              onTap: () {
                showAllButton = true;
                setState(() {});
              },
              child: showAllButton
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(top: 400).r,
                      child: Image.asset(
                        'assets/images/showAllProduct.png',
                        width: 1.sw,
                        height: 310.h,
                        fit: BoxFit.cover,
                      ),
                    ),
            ),
            showAllButton
                ? const SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(top: 500).r,
                    child: Column(
                      children: [
                        widget.isProductFrom
                            ? const ProductFrom(
                                image: 'assets/images/costum.png',
                                productName:
                                    'Спортивный костюм Adidas Originals 1221',
                                price: '27 600',
                              )
                            : const SizedBox(),
                        SizedBox(height: 40.h),
                        Image.asset(
                          'assets/images/${widget.advertising}.png',
                          width: 343.w,
                          height: 56.h,
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                  ),
          ],
        ),
        showAllButton
            ? Center(
                child: IconButton(
                  onPressed: () {
                    showAllButton = false;
                    setState(() {});
                  },
                  icon: Container(
                    width: 36.w,
                    height: 36.h,
                    decoration: BoxDecoration(
                      color: ThemeHelper.primaryOrange,
                      borderRadius: BorderRadius.circular(50.r),
                    ),
                    child: const Icon(
                      Icons.keyboard_double_arrow_up_outlined,
                      color: ThemeHelper.white,
                    ),
                  ),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
