/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/main_screen.dart';
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class MyBottomNavigationBar extends StatefulWidget {
  const MyBottomNavigationBar({super.key});

  @override
  State<MyBottomNavigationBar> createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<Widget> _bottomBarItems = const [
    MainScreen(),
    MainScreen(),
    MainScreen(),
    MainScreen(),
    MainScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Scaffold(
      body: _bottomBarItems.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        useLegacyColorScheme: false,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: TextHelper.fS10FW500.copyWith(
          color: ThemeHelper.primaryOrange,
        ),
        unselectedLabelStyle: TextHelper.fS10FW500.copyWith(
          color: ThemeHelper.grey,
        ),
        iconSize: 50,
        unselectedIconTheme: const IconThemeData(color: ThemeHelper.grey),
        selectedIconTheme: const IconThemeData(
          color: ThemeHelper.primaryOrange,
          size: 50,
        ),
        items: [
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/homeIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/homeIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.grey,
              ),
            ),
            label: "Главная",
          ),
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/catalogIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/catalogIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.grey,
              ),
            ),
            label: "Каталог",
          ),
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/qrIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/qrIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.grey,
              ),
            ),
            label: "Мой QR",
          ),
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/marketIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/marketIcon.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.grey,
              ),
            ),
            label: "Корзина",
          ),
          BottomNavigationBarItem(
            activeIcon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/profile.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              ),
            ),
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 8).r,
              child: Image.asset(
                'assets/images/profile.png',
                width: 20.w,
                height: 18.h,
                color: ThemeHelper.grey,
              ),
            ),
            label: "Профиль",
          ),
        ],
      ),
    );
  }
}
