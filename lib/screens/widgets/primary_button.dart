/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class PrimaryButton extends StatelessWidget {
  final String title;
  final TextStyle? textStyle;
  final double? width;
  final double? height;

  const PrimaryButton({
    super.key,
    this.width,
    this.height,
    required this.title,
    this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          textStyle: textStyle!,
          backgroundColor: ThemeHelper.primaryOrange,
        ),
        onPressed: () {},
        child: Text(title),
      ),
    );
  }
}
