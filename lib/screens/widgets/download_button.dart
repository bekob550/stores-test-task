/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class DownloadButton extends StatelessWidget {
  final String image;

  const DownloadButton({super.key, required this.image});

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          fixedSize: Size(1.sw, 40.h),
          backgroundColor: ThemeHelper.downloadButton),
      onPressed: () {},
      child: Image.asset(
        'assets/images/$image.png',
        width: 104.w,
        height: 28.h,
      ),
    );
  }
}
