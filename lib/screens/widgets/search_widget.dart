/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({super.key});

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Container(
      decoration: BoxDecoration(
        color: ThemeHelper.white,
        borderRadius: BorderRadius.circular(12).r,
        boxShadow: [
          BoxShadow(
            blurRadius: 2.5,
            spreadRadius: 1.4,
            color: ThemeHelper.grey.withOpacity(0.09),
          ),
        ],
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Искать в Stores ...',
          hintStyle: TextHelper.fS12FW500.copyWith(
            color: ThemeHelper.hintColor,
          ),
          prefixIcon: Icon(
            Icons.search,
            size: 30.r,
            color: ThemeHelper.primaryOrange,
          ),
        ),
      ),
    );
  }
}
