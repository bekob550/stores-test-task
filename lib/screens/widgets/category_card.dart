/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class CategoryCard extends StatelessWidget {
  final double width;
  final double height;
  final String title;
  final String image;

  const CategoryCard({
    super.key,
    this.width = 77,
    this.height = 77,
    required this.title,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(6).r,
          margin: const EdgeInsets.only(
            right: 12,
            bottom: 8,
          ).r,
          width: width,
          height: height,
          decoration: BoxDecoration(
            color: ThemeHelper.white,
            borderRadius: BorderRadius.circular(12.r),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 0),
                blurRadius: 4.r,
                color: ThemeHelper.grey.withOpacity(0.15),
              )
            ],
          ),
          child: Image.asset('assets/images/$image.png'),
        ),
        SizedBox(
          width: width,
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextHelper.fS10FW500.copyWith(
              color: ThemeHelper.black,
            ),
          ),
        ),
      ],
    );
  }
}
