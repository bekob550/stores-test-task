/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/widgets/download_button.dart';
import 'package:stores_test_task/screens/widgets/mark_down.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class BottomMenuWidget extends StatelessWidget {
  const BottomMenuWidget({super.key});

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Container(
      padding: const EdgeInsets.only(
        left: 16,
        right: 13,
        top: 26,
        bottom: 21,
      ).r,
      width: 1.sw,
      color: ThemeHelper.menuBlack,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            'assets/images/storesBrandbook.png',
            width: 127.66.w,
            height: 30.h,
          ),
          SizedBox(height: 33.h),
          const Divider(
            color: ThemeHelper.hintColor,
            height: 1,
          ),
          ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return MarkDownWidget(
                title: 'Маркетплейс',
                height: index == 2 ? 13 : 0,
              );
            },
            itemCount: 3,
          ),
          const DownloadButton(image: 'appStore'),
          const DownloadButton(image: 'googlePlay'),
        ],
      ),
    );
  }
}
