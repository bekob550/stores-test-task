/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class ProductCard extends StatelessWidget {
  final String image;
  final String title;
  final int price;
  final bool isSalePrice;
  final bool isCashBack;
  final String? cashBackIcon;
  final bool isShopName;
  final String? salePrice;

  const ProductCard({
    super.key,
    this.isCashBack = false,
    this.isSalePrice = false,
    this.isShopName = false,
    this.cashBackIcon,
    required this.image,
    required this.title,
    required this.price,
    this.salePrice,
  });

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Container(
      width: 167.w,
      height: 256.h,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              offset: const Offset(0, 0),
              blurRadius: 4.r,
              color: ThemeHelper.grey.withOpacity(0.10))
        ],
        color: ThemeHelper.white,
        borderRadius: BorderRadius.circular(12.r),
      ),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.vertical(top: Radius.circular(12.r)),
            child: Image.asset(
              'assets/images/$image.png',
              width: 167.w,
              height: 148.h,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            left: 145.w,
            top: 3.h,
            child: Icon(
              Icons.favorite_border_outlined,
              size: 20.r,
              color: ThemeHelper.hintColor,
            ),
          ),
          Positioned(
            top: 158.h,
            left: 8.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 140.w,
                  child: Text(
                    title,
                    style: TextHelper.fS12FW500.copyWith(
                      color: ThemeHelper.black,
                    ),
                  ),
                ),
                SizedBox(height: 9.h),
                isShopName
                    ? Text(
                        'Softech.kg',
                        style: TextHelper.fS8FW300.copyWith(
                          color: ThemeHelper.hintColor,
                        ),
                      )
                    : const Text(''),
                isCashBack
                    ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4).r,
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/images/cashBack.png',
                              width: 12.w,
                              height: 12.h,
                            ),
                            SizedBox(width: 4.w),
                            Text(
                              '1299',
                              style: TextHelper.fS10FW400.copyWith(
                                color: ThemeHelper.primaryOrange,
                              ),
                            )
                          ],
                        ),
                      )
                    : const Text(''),
                isSalePrice
                    ? Row(
                        children: [
                          Text(
                            salePrice ?? '',
                            style: TextHelper.fS12FW500.copyWith(
                              decoration: TextDecoration.lineThrough,
                              color: ThemeHelper.hintColor,
                            ),
                          ),
                          Text(
                            'c',
                            style: TextHelper.fS12FW500.copyWith(
                              decoration: TextDecoration.underline,
                              color: ThemeHelper.hintColor,
                            ),
                          ),
                        ],
                      )
                    : const Text(''),
                Row(
                  children: [
                    Text(
                      '${price.toString()} ',
                      style: TextHelper.fS18FW600.copyWith(
                        color: ThemeHelper.black,
                      ),
                    ),
                    Text(
                      'c',
                      style: TextHelper.fS18FW600.copyWith(
                        decoration: TextDecoration.underline,
                        color: ThemeHelper.black,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            top: 230.h,
            left: 128.w,
            child: IconButton(
              onPressed: null,
              icon: Container(
                padding: const EdgeInsets.all(6).r,
                width: 36.w,
                height: 36.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50.r),
                  border: Border.all(
                    width: 1,
                    color: ThemeHelper.primaryOrange,
                  ),
                ),
                child: Image.asset(
                  'assets/images/marketIcon.png',
                  width: 17.w,
                  height: 17.h,
                  color: ThemeHelper.primaryOrange,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
