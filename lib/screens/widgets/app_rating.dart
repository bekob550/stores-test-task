/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class AppRatingWidget extends StatelessWidget {
  const AppRatingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Row(
      children: [
        RatingBar.builder(
            initialRating: 5,
            itemSize: 18.r,
            itemBuilder: (context, index) {
              return Image.asset(
                'assets/images/star.png',
                width: 18.w,
                height: 18.h,
                color: ThemeHelper.primaryOrange,
              );
            },
            onRatingUpdate: (rate) {}),
        SizedBox(width: 4.w),
        Text(
          '4,6',
          style: TextHelper.fS16FW400.copyWith(
            color: ThemeHelper.black,
          ),
        ),
      ],
    );
  }
}
