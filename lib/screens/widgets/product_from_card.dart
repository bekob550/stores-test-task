/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/widgets/primary_button.dart';
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class ProductFrom extends StatelessWidget {
  final String productName;
  final String price;
  final bool isSalePrice;
  final String image;

  const ProductFrom({
    super.key,
    this.isSalePrice = false,
    required this.productName,
    required this.price,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(
            top: 28,
            bottom: 22,
            left: 36,
          ).r,
          width: 307.w,
          height: 128.h,
          decoration: BoxDecoration(
            color: ThemeHelper.white,
            borderRadius: BorderRadius.circular(12.r),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 0),
                blurRadius: 4.r,
                color: ThemeHelper.grey.withOpacity(0.12),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            top: 22,
            left: 18,
            bottom: 22,
          ).r,
          width: 307.w,
          height: 140.h,
          decoration: BoxDecoration(
            color: ThemeHelper.white,
            borderRadius: BorderRadius.circular(12.r),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 0),
                blurRadius: 4.r,
                color: ThemeHelper.grey.withOpacity(0.12),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(
            top: 16,
            bottom: 22,
          ).r,
          width: 307.w,
          height: 152.h,
          decoration: BoxDecoration(
            color: ThemeHelper.white,
            borderRadius: BorderRadius.circular(12.r),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 0),
                blurRadius: 4.r,
                color: ThemeHelper.grey.withOpacity(0.12),
              ),
            ],
          ),
          child: Row(
            children: [
              Image.asset(
                image,
                width: 120.w,
                height: 120.h,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 10).r,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 125.w,
                      child: Text(
                        productName,
                        style: TextHelper.fS12FW500.copyWith(
                          color: ThemeHelper.grey,
                        ),
                      ),
                    ),
                    SizedBox(height: 10.h),
                    isSalePrice
                        ? Text(
                            '116 700 c',
                            style: TextHelper.fS14FW500.copyWith(
                              color: ThemeHelper.grey,
                              decoration: TextDecoration.lineThrough,
                            ),
                          )
                        : const SizedBox(),
                    Row(
                      children: [
                        Text(
                          price,
                          style: TextHelper.fS18FW600.copyWith(
                            color: ThemeHelper.black,
                          ),
                        ),
                        Text(
                          'c',
                          style: TextHelper.fS18FW600.copyWith(
                            color: ThemeHelper.black,
                            decoration: TextDecoration.underline,
                          ),
                        )
                      ],
                    ),
                    const Spacer(),
                    PrimaryButton(
                      title: 'В корзину',
                      width: 121.w,
                      height: 20.h,
                      textStyle: TextHelper.fS14FW500.copyWith(
                        color: ThemeHelper.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
