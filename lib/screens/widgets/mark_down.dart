/* External dependencies */
import 'package:flutter/material.dart';

/* Local dependencies */
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class MarkDownWidget extends StatelessWidget {
  final String title;
  final double? height;

  const MarkDownWidget({
    super.key,
    required this.title,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Column(
      children: [
        Row(
          children: [
            Text(
              title,
              style: TextHelper.fS15FW600.copyWith(
                color: ThemeHelper.infoTextColor,
              ),
            ),
            const Spacer(),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.keyboard_arrow_down_outlined,
                color: ThemeHelper.infoTextColor,
              ),
            ),
          ],
        ),
        const Divider(
          color: ThemeHelper.hintColor,
          height: 0,
        ),
        SizedBox(height: height),
      ],
    );
  }
}
