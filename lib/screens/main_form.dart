/* External dependencies */
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/* Local dependencies */
import 'package:stores_test_task/screens/helpers/category_list.dart';
import 'package:stores_test_task/screens/helpers/product_list.dart';
import 'package:stores_test_task/screens/widgets/app_rating.dart';
import 'package:stores_test_task/screens/widgets/bottom_menu_widget.dart';
import 'package:stores_test_task/screens/widgets/category_card.dart';
import 'package:stores_test_task/screens/widgets/primary_button.dart';
import 'package:stores_test_task/screens/widgets/product_from_card.dart';
import 'package:stores_test_task/screens/widgets/product_grid_tile.dart';
import 'package:stores_test_task/screens/widgets/search_widget.dart';
import 'package:stores_test_task/theme/app_style.dart';
import 'package:stores_test_task/theme/colors.dart';
import 'package:stores_test_task/theme/constants.dart';

class MainForm extends StatefulWidget {
  const MainForm({super.key});

  @override
  State<MainForm> createState() => _MainFormState();
}

class _MainFormState extends State<MainForm> {
  bool showAllButton = false;

  @override
  Widget build(BuildContext context) {
    MyScreenUtil.init(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 17,
                right: 15,
              ).r,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5, 59, 2, 16).r,
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/appLogo.png',
                          width: 52.w,
                          height: 50.h,
                        ),
                        SizedBox(width: 3.w),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Приложение Stores.kg',
                              style: TextHelper.fS13FW400.copyWith(
                                color: ThemeHelper.storesBlack,
                              ),
                            ),
                            SizedBox(height: 9.h),
                            const AppRatingWidget(),
                          ],
                        ),
                        const Spacer(),
                        PrimaryButton(
                          width: 107.w,
                          height: 28.h,
                          title: 'Открыть',
                          textStyle: TextHelper.fS15FW600.copyWith(
                            color: ThemeHelper.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SearchWidget(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15).r,
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/shapeIcon.png',
                          width: 16.r,
                          height: 18.r,
                          color: ThemeHelper.black,
                        ),
                        SizedBox(width: 10.w),
                        Text(
                          'Бишкек',
                          style: TextHelper.fS12FW500.copyWith(
                            color: ThemeHelper.black,
                          ),
                        ),
                        const Spacer(),
                        Text(
                          'Укажите адрес доставки',
                          style: TextHelper.fS12FW500.copyWith(
                            color: ThemeHelper.primaryOrange,
                          ),
                        ),
                      ],
                    ),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(3.42.r),
                    child: Image.asset(
                      'assets/images/appBanner.png',
                      width: 343.w,
                      height: 104.h,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(height: 23.h),
                  SizedBox(
                    height: 120.h,
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: listOfCategory.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return CategoryCard(
                            width: 77.w,
                            height: 77.h,
                            title: listOfCategory[index].title,
                            image: listOfCategory[index].image,
                          );
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 22,
                      bottom: 16,
                    ).r,
                    child: Text(
                      'Хит родаж',
                      style: TextHelper.fS16FW600.copyWith(
                        color: ThemeHelper.black,
                      ),
                    ),
                  ),
                  ProductGridTile(
                    advertising: 'Advertising1',
                    listOfProducts: listOfProducts1,
                  ),
                  Text(
                    'Продукты Apple',
                    style: TextHelper.fS16FW600.copyWith(
                      color: ThemeHelper.black,
                    ),
                  ),
                  const ProductFrom(
                    image: 'assets/images/iphone13proMax.png',
                    productName: 'iPhone 13 Pro Max 256 Gb',
                    price: '113 000',
                    isSalePrice: true,
                  ),
                  Text(
                    'Новинки',
                    style: TextHelper.fS16FW600.copyWith(
                      color: ThemeHelper.black,
                    ),
                  ),
                  SizedBox(height: 16.h),
                  ProductGridTile(
                    advertising: 'advertising2',
                    listOfProducts: listOfProducts2,
                  ),
                  Text(
                    'Всё для детей',
                    style: TextHelper.fS16FW600.copyWith(
                      color: ThemeHelper.black,
                    ),
                  ),
                  SizedBox(height: 16.h),
                  SizedBox(
                    height: 120.h,
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: listForKidsCategory.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return CategoryCard(
                            width: 68.w,
                            height: 68.h,
                            title: listForKidsCategory[index].title,
                            image: listForKidsCategory[index].image,
                          );
                        }),
                  ),
                  SizedBox(height: 22.h),
                  Text(
                    'Всё от Xiaomi',
                    style: TextHelper.fS16FW600.copyWith(
                      color: ThemeHelper.black,
                    ),
                  ),
                  SizedBox(height: 16.h),
                  ProductGridTile(
                    advertising: 'advertising3',
                    isProductFrom: true,
                    listOfProducts: listOfProducts3,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 22,
                      bottom: 16,
                    ).r,
                    child: Text(
                      'Мужская одежда',
                      style: TextHelper.fS16FW600.copyWith(
                        color: ThemeHelper.black,
                      ),
                    ),
                  ),
                  SizedBox(height: 16.h),
                  SizedBox(
                    height: 120.h,
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: listMansCategory.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return CategoryCard(
                            width: 77.w,
                            height: 77.h,
                            title: listMansCategory[index].title,
                            image: listMansCategory[index].image,
                          );
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 23,
                      bottom: 16,
                    ).r,
                    child: Text(
                      'Горячие скидки',
                      style: TextHelper.fS16FW600.copyWith(
                        color: ThemeHelper.black,
                      ),
                    ),
                  ),
                  ProductGridTile(
                    advertising: 'advertising4',
                    listOfProducts: listOfProducts4,
                  ),
                ],
              ),
            ),
            const BottomMenuWidget(),
          ],
        ),
      ),
    );
  }
}
