import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextHelper {
  static TextStyle fS8FW300 = TextStyle(
    fontSize: 8.sp,
    fontWeight: FontWeight.w300,
  );
   static TextStyle fS8FW500 = TextStyle(
    fontSize: 8.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle fS10FW400 = TextStyle(
    fontSize: 10.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle fS10FW500 = TextStyle(
    fontSize: 10.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle fS12FW500 = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle fS13FW400 = TextStyle(
    fontSize: 13.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle fS14FW500 = TextStyle(
    fontSize: 14.sp,
    fontWeight: FontWeight.w500,
  );
  static TextStyle fS15FW600 = TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w600,
  );
   static TextStyle fS16FW400 = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w400,
  );
  static TextStyle fS16FW600 = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
  );
  static TextStyle fS18FW600 = TextStyle(
    fontSize: 18.sp,
    fontWeight: FontWeight.w600,
  );
}
