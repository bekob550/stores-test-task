/* External depnedncies */
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeHelper {
  static const appColor = Color(0xffE9E4E4);
  static const primaryOrange = Color(0xffF58735);
  static const black = Color(0xff000000);
  static const white = Color(0xffFFFFFF);
  static const storesBlack = Color(0xff1E1E1E);
  static const hintColor = Color(0xffB3B3B7);
  static const grey = Color(0xff666666);
  static const pink = Color(0xff6E3CBC);
  static const infoTextColor = Color(0xffFAFAFA);
  static const menuBlack = Color(0xff333333);
  static const downloadButton = Color(0xff474747);
}

final defaultTheme = ThemeData(
  primarySwatch: Colors.blue,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  splashColor: ThemeHelper.appColor.withOpacity(0.3),
  textTheme: GoogleFonts.montserratTextTheme(
    ThemeData.light().textTheme.apply(
          bodyColor: ThemeHelper.appColor,
          displayColor: ThemeHelper.appColor,
        ),
  ),
  appBarTheme: const AppBarTheme(color: ThemeHelper.appColor),
);
